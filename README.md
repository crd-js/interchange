# Interchange

Interchange is an image switcher for responsive web pages. Interchange extends CRD.BreakpointSpy, and hooks to its events, to change images sources (and background images) when webpage breakpoint changes. It can also change image sources in case of HDPI (or Retina) screens, an apply different image sources for the case - prefixing the images with ```@2x``` by default -.

### Example

For each image that the image switcher must watch:

```html
<img src="default.jpg" interchange-data="{ 'xs' : 'image-xs.jpg', 'sm' : 'image-sm.jpg', 'md' : 'image-md.jpg', 'lg' : 'image-lg.jpg' }">
```

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="breakpoint-spy.js"></script>
<script type="text/javascript" src="bootstrap-css-rules.js"></script>
<script type="text/javascript" src="interchange.js"></script>
```

Then just initiate the image switcher script:

```javascript
// Initializes image switcher
var spy = new CRD.Interchange();
```

On ```domready```, CRD.Interchange will parse the ```interchange-data``` value (as JSON data), and replace the image source with the specified source for the breakpoint.

In case of using custom rules (see CRD.BreakpointSpy documentation), the properties names for the JSON data parsed must match the defined custom rules names.

*A packed and merged source (including function.js, utils.js, breakpoint-spy.js, bootstrap-css-rules.js and interchange.js) is available to use.*

### Arguments

```CRD.Interchange``` accepts a single argument:

Argument | Default | Description
-------- | ------- | -----------
options : ```Object``` | ```{ 'selector' : '[interchange-data]', 'usehdpi' : false, 'hidpisuffix' : '@2x', 'rules' : [] }``` | Options to overwrite default values for breakpoint selector, usage of hi-dpi images, hi-dpi image prefix and rules.

### Settings

Option | Type | Default | Description
------ | ---- | ------- | -----------
selector | ```String``` | ```'[interchange-data]'``` | CSS selector for all the elements that the image switch should apply.
usehdpi | ```Boolean``` | ```false``` | Append suffix on image file names when screen or device is hdpi (or retina)?
selector | ```String``` | ```'@2x'``` | Suffix for image file names, when hdpi mode is enabled (changes image.jpg to image@2x.jpg).
rules | ```Array``` | ```[]``` | Set of rules to apply to  the inherited ```CRD.BreakpointSpy```.

### Events

Event | Params | Description
----- | ------ | -----------
```crd.interchange.before``` | object : ```CRD.Interchange``` | Fired before changing image and background sources
```crd.interchange.after``` | object : ```CRD.Interchange``` | Fired after changing image and background sources
```crd.interchange.change``` | source : ```String```, object : ```CRD.Interchange``` | Fired on each element (binded to the element) that the source (or background) switch is successfully applyed
```crd.interchange.fail``` | object : ```CRD.Interchange``` | Fired on each element (binded to the element) that the source (or background) switching failed

### Methods

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | options : ```Object``` | A reference to the current object or class: ```CRD.Interchange```. | Initializes the instance with the provided options.
changeSources | --- | --- | Apply the function to change element sources to all matched elements.
changeSource | element : ```Element``` | --- | Change source for a specific element.
datherData | element : ```Element``` | And ```Object``` contaaining all the  matched data for the defined breakpoints. | Gather breakpoint data for a specific element.
updateElement | element : ```Element```, src : ```String``` | --- | Change the element source or background image.
destroy | --- | --- | Calls ```CRD.breakpointSpy.destroy``` and unhooks from ```CRD.breakpointSpy``` events.

### Customization

```CRD.Interchange``` accepts customization over the way data is gathered for the different image sources.

As an alternative to the ```interchange-data``` implementation, a ```data-attribute``` based option is provided.

After including the ```interchange.js``` script:

```html
<script type="text/javascript" src="data-attributed.js"></script>
```

And for each image that the image switcher must watch:

```html
<img src="default.jpg" 
	data-image-mobile="image-xs.jpg" 
	data-image-tablet="image-sm.jpg" 
	data-image-desktop="image-md.jpg" 
	data-image-desktop-large="image-lg.jpg">
```

Alternative implementations can be written. To do so, ```gatherData``` function must be overwritten. After including the ```interchange.js``` file, add your own implementation (before initializing the image switcher).

The alternative implementation function, must return an object containing all the properties (defined rule names for the ```CRD.BreakpointSpy```) with its corresponding image sources:

```json
{
	"xs" : "image-xs.jpg",
	"sm" : "image-sm.jpg",
	"md" : "image-md.jpg",
	"lg" : "image-lg.jpg"
}
```

#### Example

```javascript
// Overwrites original function
CRD.Interchange.prototype.gatherData = function(element) {
	// Retun object
};
```

### Browser Support

See ```CRD.BreakpointSpy``` documentation for browser support.

### Dependencies

- CRD.Utils
- CRD.BreakpointSpy
- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
